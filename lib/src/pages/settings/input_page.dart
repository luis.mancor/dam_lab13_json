import 'package:flutter/material.dart';

class InputPage extends StatelessWidget {

  final options = ['Uno','Dos','Tres','Cuatro','Cinco'];
  //TextStyle styleText = TextStyle(color:Colors.red, fontSize: 25.0);
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Input Page'),
      ),

      /*body: Center(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            new TextFormField(
                    decoration: const InputDecoration(
                      icon: const Icon(Icons.person),
                      hintText: 'Enter your first and last name',
                      labelText: 'Name',
                    ),
                  ),
            
            Icon(Icons.beach_access, color: Colors.blue, size:36.0),
          ],
        ),
      ),
      */
      body: Center(
        
        child: Column(
          children: <Widget>[
            TextField(
              decoration: const InputDecoration(
                          icon: const Icon(Icons.person),
                          hintText: 'Enter your first and last name',
                          labelText: 'Name',
                        ),
            ),

            TextField(
              decoration: const InputDecoration(
                          icon: const Icon(Icons.contacts),
                          hintText: 'Ingrese su apellido',
                          labelText: 'Apellido',
                        ),
            ),
            TextField(
              decoration: const InputDecoration(
                          icon: const Icon(Icons.phone),
                          hintText: 'Ingrese su celular',
                          labelText: 'Celular',
                        ),
            ),

          ],
        ),
        
      ),
      
      floatingActionButton: FloatingActionButton(
          child: Icon(Icons.keyboard_backspace),
          onPressed: () => {
            Navigator.pop(context),
          },
        ),

    );
  }
}