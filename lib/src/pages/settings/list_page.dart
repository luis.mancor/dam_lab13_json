import 'package:flutter/material.dart';

class ListPage extends StatelessWidget {

  final options = ['Uno','Dos','Tres','Cuatro','Cinco'];
  //TextStyle styleText = TextStyle(color:Colors.red, fontSize: 25.0);
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('List Page'),
      ),

      body: ListView(
        children: const <Widget>[
          
          Card(
            child: ListTile(
              leading: FlutterLogo(),
              title: Text('One-line with leading widget'),
              trailing: Icon(Icons.more_vert),
            ),
          ),
          Card(
            child: ListTile(
              leading: FlutterLogo(),
              title: Text('One-line with both widgets'),
              trailing: Icon(Icons.more_vert),
            ),
          ),
          Card(
            child: ListTile(
              title: Text('Listas amplias'),
              dense: true,
            ),
          ),
          Card(
            child: ListTile(
              leading: FlutterLogo(size: 56.0),
              title: Text('Two-line ListTile'),
              subtitle: Text('Here is a second line'),
              trailing: Icon(Icons.more_vert),
            ),
          ),
          Card(
            child: ListTile(
              leading: FlutterLogo(size: 56.0),
              title: Text('Two-line ListTile'),
              subtitle: Text('Here is a second line'),
              trailing: Icon(Icons.more_vert),
            ),
          ),
         
        ],
      ),
      
      floatingActionButton: FloatingActionButton(
          child: Icon(Icons.keyboard_backspace),
          onPressed: () => {
            Navigator.pop(context),
          },
        ),

    );
  }
}