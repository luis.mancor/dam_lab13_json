import 'package:flutter/material.dart';

class AlertPage extends StatelessWidget {

  final options = ['Uno','Dos','Tres','Cuatro','Cinco'];
  //TextStyle styleText = TextStyle(color:Colors.red, fontSize: 25.0);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Alert Page'),
      ),

      body: Column(
        children: <Widget>[
          MyCardWidget(),
          MyCardWidget(),
      ],),
      
      floatingActionButton: FloatingActionButton(
          child: Icon(Icons.keyboard_backspace),
          onPressed: () => {
            Navigator.pop(context),
          },
        ),

    );
  }
}

//Card
class MyCardWidget extends StatelessWidget {
  //MyCardWidget({Key key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Card(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          
          children: <Widget>[
            //AssetImage(assetName),
            const Image(
              image: NetworkImage('https://mensajesdios.com/wp-content/uploads/2017/02/african-sunset-tanzania-africa.jpg'),
              ),
            const ListTile(
              leading: Icon(Icons.album),
              title: Text('The Enchanted Nightingale'),
              subtitle: Text('Music by Julie Gable. Lyrics by Sidney Stein.'),
            ),
            ButtonBar(
              children: <Widget>[
                FlatButton(
                  child: const Text('BUY TICKETS'),
                  onPressed: () {/* ... */},
                ),
                FlatButton(
                  child: const Text('LISTEN'),
                  onPressed: () {/* ... */},
                ),
              ],
            ),
          ],
        ),
      );
  }
}