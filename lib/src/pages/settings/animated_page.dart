import 'package:flutter/material.dart';

class AnimatedPage extends StatelessWidget {

  final options = ['Uno','Dos','Tres','Cuatro','Cinco'];
  //TextStyle styleText = TextStyle(color:Colors.red, fontSize: 25.0);
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Animated Page'),
      ),

      body: Center(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Icon(Icons.adb, color: Colors.red, size: 50.0),
            Icon(Icons.beach_access, color: Colors.blue, size:36.0),
          ],
        ),
      ),
      
      floatingActionButton: FloatingActionButton(
          child: Icon(Icons.keyboard_backspace),
          onPressed: () => {
            Navigator.pop(context),
          },
        ),

    );
  }
}