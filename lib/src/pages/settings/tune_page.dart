import 'package:flutter/material.dart';

class TunePage extends StatelessWidget {

  final options = ['Uno','Dos','Tres','Cuatro','Cinco'];
  //TextStyle styleText = TextStyle(color:Colors.red, fontSize: 25.0);
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Slider Page'),
      ),

      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Icon(Icons.adb, color: Colors.red, size: 50.0),
            Text('Primer Slider'),
            Slider(
              min: 0,
              max: 100,
              value: 0,//_value,
              onChanged: (value) {
                //setState(() { _value = value;});
              },
            ),
            Text('Segundo Slider'),
            Slider(
              min: 0,
              max: 100,
              value: 0,//_value,
              onChanged: (value) {
                //setState(() { _value = value;});
              },
            ),

          ],
        ),
      ),
      
      floatingActionButton: FloatingActionButton(
          child: Icon(Icons.keyboard_backspace),
          onPressed: () => {
            Navigator.pop(context),
          },
        ),

    );
  }
}